This is a demo repo, used to tests all things about gitlab

Testing tag creation

This repo was used to test this deployment script [https://gitlab.com/doublegdp/deplot-it](https://gitlab.com/doublegdp/deplot-it)

Steps:
1. create different issues 
2. assign them to a user
3. give them a staging:verified label
4. used the linked script to deploy
5. example of a readable release note generated is here [https://gitlab.com/olivierjmm/gitlab-next-ci/-/releases](https://gitlab.com/olivierjmm/gitlab-next-ci/-/releases)
